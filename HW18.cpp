﻿// HW18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

class Rating
{
private:
	int score{};
	std::string name;
public:
	void setScore(int s)
	{
		this->score = s;
	}
	void setName(std::string n)
	{
		this->name = n;
	}
	int getScore()
	{
		return this->score;
	}
	std::string getName()
	{
		return this->name;
	}
};

void sort(Rating* array, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j=0;j<size-i;j++)
		{
			if (array[j].getScore() < array[j + 1].getScore())
			{
				Rating t = array[j];
				array[j] = array[j + 1];
				array[j + 1] = t;
			}
		}
	}
}

int main()
{
	int playerCount = 0;
	std::cout << "The number of players: ";
	std::cin >> playerCount;
	Rating* players = new Rating[playerCount];

	for (int i = 0; i < playerCount; i++)
	{
		int score;
		std::string name;

		std::cout << "Name: ";
		std::cin >> name;

		std::cout << "Score: ";
		std::cin >> score;

		players[i] = Rating();
		players[i].setName(name);
		players[i].setScore(score);
	}
	sort(players, playerCount);

	for (int i = 0; i < playerCount; i++)
	{
		std::cout << players[i].getName() << " " << players[i].getScore() << std::endl;
	}
	delete[] players;
	return 0;
}
